class AppOfTheYear {
    String? name;
    String? category;
    String? developer;
    int? yearWon;

    String capsName(var name){
      return (name)!.toUpperCase();
    }
}

void main(){
  var aoy2013 = new AppOfTheYear();

  aoy2013.name = 'FNB Banking App';
  aoy2013.category = 'Banking';
  aoy2013.developer = 'FNB Banking App Team';
  aoy2013.yearWon = 2013;

  print('App Name: ${aoy2013.name}');
  print('Category: ${aoy2013.category}');
  print('Developer: ${aoy2013.developer}');
  print('Year Won: ${aoy2013.yearWon}');

  print(AppOfTheYear().capsName(aoy2013.name));
}